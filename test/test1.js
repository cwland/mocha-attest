require('dotenv').config();
const { Builder, By, Key, until } = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const assert = require('chai').assert;
const options = new chrome.Options();

options.addArguments(
  // 'headless',
  // 'disable-gpu',
  '--log-level=3'
);

describe('testing DQU', () => {
  let driver;

  before(async () => {
    driver = await new Builder()
        .forBrowser('chrome')
        .setChromeOptions(options)
        .build();
  });

  it('should have the correct title', async () => {
    await driver.get('https://dequeuniversity.com/');
    const expectedTitle = 'Deque University: Web Accessibility Training and Courses';
    const title = await driver.getTitle();
    assert.strictEqual(title, expectedTitle, 'Title is incorrect.');
  });

  it('should allow members to log in with password', async () => {
    await driver.get('https://dequeuniversity.com/login');
    const username = await driver.findElement(By.css('.loginUsername'));
    const password = await driver.findElement(By.css('.loginPassword'));
    await username.sendKeys(process.env.DQU_EMAIL);
    await password.sendKeys(process.env.DQU_PASSWORD, Key.RETURN);
    await driver.wait(until.titleContains('Member Access'), 3000);
  });

  after(async () => {
    await driver.quit();
  });
});